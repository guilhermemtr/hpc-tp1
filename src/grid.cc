#include "grid.h"

#ifdef GRID_H

grid::grid(long cols, long rows):
  cols(cols), rows(rows) {
  mat = (unsigned char *) calloc(cols * rows, sizeof(unsigned char));
}

grid::grid(grid& o):
cols(o.cols), rows(o.rows) {
  mat = (unsigned char *) malloc(cols * rows * sizeof(unsigned char));
  long x,y;
  #pragma omp parallel for  num_threads(num_cpus()) private(x)
  for(y = 0; y < rows; y++) {
    for(x = 0; x < cols; x++) {
      mat[get_pos(x,y)] = o.mat[o.get_pos(x,y)];
    }
  }
}

void grid::update(grid* old) {
  unsigned char* old_mat = old->mat;
  old->set_mat(this->mat);
  this->set_mat(old_mat);
}

grid::~grid() {
  free(mat);
}


#endif // GRID_H
