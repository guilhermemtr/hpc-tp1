#include "forest.h"

#ifdef FOREST_H

forest::forest(long cols, long rows, long x, long y, int prob):
  cols(cols), rows(rows), x(x), y(y), prob(prob) {
  __log("forest::forest(%ld,%ld,%ld,%ld,%d)\n",cols,rows,x,y,prob);
  this->time = 0;
  old = new grid(cols, rows);
  curr = new grid(cols, rows);
  __log("forest::forest: created forest grids\n");
  //changes the current to set pos (x,y) burning
  curr->mat[curr->get_pos(x,y)] = 1;
  __log("forest::forest: set (%ld,%ld) burning\n", x, y);
  save_states_to_file = false;
}

void forest::burn(long tc, long tr) {
  __log("forest::burn: starting to burn (%ld, %ld)\n", tc, tr);
  int changed;
  __log("forest::burn: number of blocks: (%ld,%ld)\n", cols, rows);
  long c_block_num = cols/tc;
  long r_block_num = rows/tr;
  long n_blocks = c_block_num * r_block_num;
  __log("forest::burn: number of blocks: (%ld,%ld)\n", c_block_num, r_block_num);
  #pragma omp parallel num_threads(num_cpus())
  do {
    #pragma omp barrier
    #pragma omp single
    {
      changed = 0;
      if(save_states_to_file) {
        __log("forest::burn: saving current step (%ld) to history\n", time);
        if(forest_history.size() *
            sizeof(unsigned char) * 
            curr->cols * 
            curr-> rows >
            SZ_THRESH
            ) {
          printf("flushing\n");
          this->flush();
        }
        if(!(time % save_step))
          forest_history[time] = new grid(*curr);
      }
      __log("forest::burn: time:%ld\n", time);
      curr->update(old);
      __log("forest::burn: replaced old grid with current\n");
      //no problem of updating time here since it is not used until next
      //iteration
      time++;
    }

    long cur_block;
    #pragma omp barrier
    #pragma omp for schedule(static, GRAIN_SIZE)
    for(cur_block = 0; cur_block < n_blocks; cur_block++) {
      long x,y;
      long beg_y = (cur_block/c_block_num) * tr;
      long end_y = beg_y + tr;
      long beg_x = (cur_block%c_block_num) * tc;
      long end_x = beg_x + tc;

      for(y = beg_y; y < end_y; y++) {
        for(x = beg_x; x < end_x; x++) {
          if(x > 0 && y > 0 && y < rows - 1 && x < cols - 1) {
            if(old->mat[old->get_pos(x,y)] == 2) {
              curr->mat[curr->get_pos(x,y)] = 2;
            } else if(old->mat[old->get_pos(x,y)] == 1) {
              curr->mat[curr->get_pos(x,y)] = 2;
              changed = 1;
            } else if(old->get_val_neighbors(1,x,y)) {
              if(rand() % 101 < prob) {
                curr->mat[curr->get_pos(x,y)] = 1;
                changed = 1;
              } else {
                curr->mat[curr->get_pos(x,y)] = 0;
              }
            }
          }
        }
      }
    }
  } while(changed);
  
  if(save_states_to_file) {
    __log("forest::burn: saving current step to history\n");
    forest_history[time] = new grid(*curr);
  }
}

void forest::burn() {
  __log("forest::burn: starting to burn\n");
  int changed;
  #pragma omp parallel num_threads(num_cpus())
  do {
    #pragma omp barrier
    #pragma omp single
    {
      changed = 0;
      if(save_states_to_file) {
        __log("forest::burn: saving current step (%ld) to history\n", time);
        if(!(time % save_step))
          forest_history[time] = new grid(*curr);
        if(forest_history.size() *
            sizeof(unsigned char) * 
            curr->cols * 
            curr-> rows >
            SZ_THRESH
            ) {
          this->flush();
        }
      }
      __log("forest::burn: time:%ld\n", time);
      curr->update(old);
      __log("forest::burn: replaced old grid with current\n");
      //no problem of updating time here since it is not used until next
      //iteration
      time++;
    }

    long x,y;
    #pragma omp barrier
    #pragma omp for private(x) schedule(static, GRAIN_SIZE)
    for(y = 1; y < curr->rows - 1; y++) {
      for(x = 1; x < curr->cols - 1; x++) {
  		  if(old->mat[old->get_pos(x,y)] == 2) {
  			 curr->mat[curr->get_pos(x,y)] = 2;
  		  } else if(old->mat[old->get_pos(x,y)] == 1) {
  		  	curr->mat[curr->get_pos(x,y)] = 2;
  		  	changed = 1;
  		  } else if(old->get_val_neighbors(1,x,y)) {
  		  	if(rand() % 101 < prob) {
  		  		curr->mat[curr->get_pos(x,y)] = 1;
  		  		changed = 1;
  		  	} else {
  		  		curr->mat[curr->get_pos(x,y)] = 0;
  		  	}
  		  }
      }
    }
  } while(changed);
  
  if(save_states_to_file) {
    __log("forest::burn: saving current step to history\n");
    forest_history[time] = new grid(*curr);
  }
}

float forest::get_burnt_area() {
	return curr->get_area(2);
}

float forest::get_burning_area() {
	return curr->get_area(1);
}

float forest::get_fire_area() {
	return this->get_burnt_area() + this->get_burning_area();
}

long forest::get_time_step() {
	return this->time;
}

grid* forest::get_step(long stime) {
  if(!save_states_to_file) {
    throw std::string("states not being saved");
  }
	if(stime < 0 || stime >= time) {
		throw std::string("check your clock please");
	}
	return forest_history[stime];
}

void forest::save_states(std::string file, long save_steps) {
  filename = file;
  save_states_to_file = true;
  this->save_step = save_steps;
}

void forest::flush() {
  auto local_it = forest_history.begin();
  while(local_it != forest_history.end()){
    if(save_states_to_file) {
      file2ppm p(*(local_it->second));
      char name[32];
      sprintf(name, "%s.ppm.%05ld", filename.c_str(), local_it->first);
      p.save_grid(name);
    }
    grid* g = local_it->second;
    delete g;
    local_it++;
  }
  forest_history.clear();
}

forest::~forest() {
  //delete the forest_history map
  flush();
  delete old;
  delete curr;
}

#endif // FOREST_H