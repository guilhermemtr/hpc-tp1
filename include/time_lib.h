#ifndef TIMELIB_H
#define TIMELIB_H

#include <chrono>

class time_lib
{
    public:
        /** Default constructor */
        time_lib();

        void start();

        void finish();

        double get_time();

        /** Default destructor */
        virtual ~time_lib();
    protected:
    private:
        std::chrono::system_clock::time_point begin;
        std::chrono::system_clock::time_point end;
};

#endif // TIMELIB_H
