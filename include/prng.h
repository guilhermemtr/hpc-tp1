#include <cstdlib>

#ifndef RANDOMNUMBERGENERATOR_H
#define RANDOMNUMBERGENERATOR_H

struct prng
{
  inline prng() {
    m_z = random();
    m_w = random();
  }

  inline unsigned int frandom() {
    m_z = 36969 * (m_z & 65535) + (m_z >> 16);
    m_w = 18000 * (m_w & 65535) + (m_w >> 16);
    return (m_z << 16) + m_w;
  }
  int m_z;
  int m_w;
};

#endif // RANDOMNUMBERGENERATOR_H
