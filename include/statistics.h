#include <stdlib.h>
#include <cmath>

#ifndef STATISTICS_H
#define STATISTICS_H

class statistics
{
  public:
    /** Default constructor */
    statistics(int n_tests);
    statistics(int n_tests, double to_discard);
    void add_value(double value);
    double get_avg();
    double get_std_dev();
    /** Default destructor */
    virtual ~statistics();
  protected:
  private:
    double to_discard;
    double * times;
    int ctr;
    int n_tests;
};

#endif // STATISTICS_H
