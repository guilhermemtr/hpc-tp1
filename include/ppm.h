#include <cstdlib>
#include "utils.h"
#include "grid.h"

#ifndef FILE_PPM_H
#define FILE_PPM_H

const unsigned char forest_colors [3][3] = 
{
  //fresh color
  {
    (unsigned char)0xBA, 
    (unsigned char)0xE0, 
    (unsigned char)0x8B
  },
  //burning color
  {
    (unsigned char)0xB5, 
    (unsigned char)0x27, 
    (unsigned char)0x00
  },
  //burnt color
  {
    (unsigned char)0xFF, 
    (unsigned char)0xB2, 
    (unsigned char)0x73
  }
};

class file2ppm
{
  public:
    /** Default constructor */
    file2ppm(long cols, long rows, unsigned char* mat);
    file2ppm(grid& g);
    file2ppm(char* name, long cols, long rows);

    void save_grid(std::string file);
    void save_grid(char* file);

    virtual ~file2ppm();
  private:
    unsigned char* mat;
    long cols, rows;
    bool mat_own = false;
};

#endif // FILE_PPM_H
