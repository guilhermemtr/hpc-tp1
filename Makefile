#!/bin/bash
CC=g++
RM=rm -f

PREFIX=.cc
OBJ=.o

SRC_PATH=src
INC_PATH=-Iinclude/

CFLAGS=-std=c++11 -Wall $(MODE) $(INC_PATH)
LDFLAGS=-lpthread -lm -lboost_iostreams -lboost_system -lboost_filesystem

SOURCES=main.cc $(SRC_PATH)/forest.cc $(SRC_PATH)/grid.cc $(SRC_PATH)/ppm.cc $(SRC_PATH)/time_lib.cc $(SRC_PATH)/statistics.cc
OBJECTS=$(SOURCES:$(PREFIX)=$(OBJ))

EXECUTABLE=fire

MODE=$(RELEASE) $(PAR)

RELEASE=-O2
DEBUG=-g -D__YAL_ON__

PAR=-fopenmp
SEQ=


GARBAGE=states/*.ppm

all: $(SOURCES) $(EXECUTABLE)
	$(CC) $(CFLAGS) $(OBJECTS) -o $(EXECUTABLE) $(LDFLAGS)

$(EXECUTABLE): $(OBJECTS)

%$(OBJ): %$(PREFIX)
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	$(RM) $(OBJECTS) $(EXECUTABLE) $(GARBAGE)
